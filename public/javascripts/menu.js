/* DO NOT MODIFY. This file was compiled Thu, 05 Apr 2012 14:29:43 GMT from
 * /Users/cgenco/Dropbox/Projects/AmericanRestoration/american_restoration/app/coffeescripts/menu.coffee
 */

(function() {
  $(document).ready(function() {
    return $("ul#menu li").hover(function() {
      return $(this).find("ul.subnav").stop(true, true).slideDown('fast').show();
    }, function() {
      return $(this).find("ul.subnav").stop(true, true).slideUp('fast').show();
    });
  });
}).call(this);
