/* DO NOT MODIFY. This file was compiled Thu, 05 Apr 2012 14:23:10 GMT from
 * /Users/cgenco/Dropbox/Projects/AmericanRestoration/american_restoration/app/coffeescripts/faq.coffee
 */

(function() {
  $(document).ready(function() {
    $('.answer').hide();
    $('#faqs h3').each(function() {
      var answer, state, tis;
      tis = $(this);
      state = false;
      answer = tis.next('div').slideUp();
      return tis.click(function() {
        state = !state;
        answer.slideToggle(state);
        return tis.toggleClass('active', state);
      });
    });
    $("#filter").show();
    return $("#filter").keyup(function() {
      var count, filter;
      filter = $(this).val();
      count = 0;
      $("#faqs h3").each(function() {
        var answer_div;
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
          $(this).hide();
          answer_div = $(this).next('div');
          if (answer_div.is(":visible")) {
            answer_div.hide();
            return answer_div.addClass('show_me_please');
          }
        } else {
          $(this).show();
          answer_div = $(this).next('div');
          if (answer_div.hasClass('show_me_please')) {
            answer_div.show();
          }
          return count++;
        }
      });
      return $("#filter-count").text(count);
    });
  });
}).call(this);
