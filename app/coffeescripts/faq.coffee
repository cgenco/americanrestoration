$(document).ready ->
  $('.answer').hide()
  $('#faqs h3').each ->
    tis = $(this)
    state = false
    answer = tis.next('div').slideUp()
    tis.click ->
      state = !state
      answer.slideToggle(state)
      tis.toggleClass('active',state)
  
  $("#filter").show()
  $("#filter").keyup ->
    filter = $(this).val() 
    count = 0
    $("#faqs h3").each ->
      if ($(this).text().search(new RegExp(filter, "i")) < 0)
        # $(this).addClass("hidden")
        $(this).hide()
        answer_div = $(this).next('div')
        if(answer_div.is(":visible"))
          answer_div.hide()
          answer_div.addClass('show_me_please')
      else
        # $(this).removeClass("hidden")
        $(this).show()
        answer_div = $(this).next('div')
        if(answer_div.hasClass('show_me_please'))
          answer_div.show()
        count++
    $("#filter-count").text(count)