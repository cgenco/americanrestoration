class TeamMember < ActiveRecord::Base
  def aka
    name.downcase.gsub(' ', '-')
  end
  
  def photo
    # return "team/#{aka}.jpg" if photo_url.nil?
    # 
    # photo_url
    # 
    File.exists?(Rails.root.join("./public/images/team/#{aka}.jpg"))? "team/#{aka}.jpg" : "team/anon.jpg"
  end
end
