class Affiliate < ActiveRecord::Base
  def domain
    return nil unless url
    url =~ /(http:\/\/)?(www.)?([^\/]*)(\.com|\.net|\.org|\.edu|\.tx\.us)/
    $3
  end

  def logo
    return self[:logo] unless domain
    default = "affiliates/#{domain}-logo.png"
    return self[:logo] if self[:logo] && !self[:logo].empty?
    File.exists?(File.join(Rails.root, "public", "images", default)) ? default : nil
  end

  def screenshot
    return self[:screenshot] unless domain
    default = "affiliates/#{domain}-screenshot.png"
    return self[:screenshot] if self[:screenshot] && !self[:screenshot].empty?
    File.exists?(File.join(Rails.root, "public", "images", default)) ? default : nil
  end
end
