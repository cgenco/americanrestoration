class Slide < ActiveRecord::Base
  has_attached_file :image, :styles => { :large => "800x800>", :medium => "400x400>", :thumb => "100x100>" }
  # want to add a new size? Use:
  # bundle exec rake paperclip:refresh CLASS=Slide
  belongs_to :servic
end
