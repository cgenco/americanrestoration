ActiveAdmin.register Faq do
  # controller.clear_sidebar_sections!
  
  index do
    column :question
    column :answer
    
    default_actions
  end
end
