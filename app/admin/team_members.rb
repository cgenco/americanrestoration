ActiveAdmin.register TeamMember do
  # controller.clear_sidebar_sections!
  # filter :name
  
  index do
    column 'Photo' do |team_member|
      image_tag team_member.photo, :height => '100px'
    end
    
    column :name
    column :position
    
    column "Bio" do |team_member|
      truncate team_member.bio, :length => 200
    end
    
    column :email_address
    column :linked_in_url
    
    default_actions
  end
end
