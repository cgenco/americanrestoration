ActiveAdmin.register Affiliate do
  # @per_page = 9999
  # controller.clear_sidebar_sections!
  
  index do
    column 'Logo' do |affiliate|
      image_tag affiliate.logo if affiliate.logo
    end
    column 'Name' do |affiliate|
      link_to affiliate.name, affiliate.url
    end
    
    default_actions
  end

  # https://github.com/justinfrench/formtastic
  form do |f|
    f.inputs "Affiliate" do
      f.input :name
      f.input :url
      f.input :description, :as => :text 
      f.input :logo
      f.input :screenshot
      f.input :category, :as => :select, :collection => Affiliate.all.map(&:category).uniq
      # f.inputs
    end

    f.buttons
  end
end
