ActiveAdmin.register Servic do #, :as => 'fancy_page' do
  # @per_page = 9999
  # controller.clear_sidebar_sections!
  index do
    column 'Name' do |service|
      service.name
    end
    column 'Description' do |service|
      BlueCloth.new(truncate(service.description, :length => 50)).to_html.html_safe
    end

    column 'Photos' do |service|
      service.slides[0...6].map{|s| image_tag(s.image.url(:thumb))}.join.html_safe +
      (service.slides.size > 6 ? "<br />...#{service.slides.size-6} more".html_safe : '')
    end
    
    default_actions
  end

  show do
    h1 servic.name
    div BlueCloth.new(servic.description).to_html.html_safe
    div servic.slides.map{|s| link_to image_tag(s.image.url(:thumb)), s.image.url(:original)}.join.html_safe
  end

form :html => {multipart: true} do |f|
  f.inputs "Service information" do
    f.input :name
    f.input :description
  end

  f.inputs "Service Illustration" do
   f.has_many :slides do |p|
     p.input :image, :as => :file, :label => "Image",:hint => p.object.image.nil? ? p.template.content_tag(:span, "No Image Yet") : p.template.image_tag(p.object.image.url(:thumb))
     p.input :description
     p.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
   end
   f.input :illustration, :label => 'OR: YouTube video HTML embed code', :as => :string
  end

  f.buttons
end
  
end
