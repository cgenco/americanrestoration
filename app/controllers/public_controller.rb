class PublicController < ApplicationController
  def index
  end

  def team
    @team_members = TeamMember.all
  end
  
  def affiliations
    @affiliations = Affiliate.all
  end
  
  def affiliate_category
    @affiliations = Affiliate.find_all_by_category(params[:category].titleize)
    render :action => 'affiliations'
  end
  
  def services
    @services = Servic.all
    @poop = "lol"
  end
  
  def show_service
    @service = Servic.find_by_name(params[:name].titleize)
    render :action => 'services'
  end
  
  def faq
    @faqs = Faq.all
  end
  
  def contact_form
  end
  
  def test
  end
  
  def coming_soon
    
  end
end
