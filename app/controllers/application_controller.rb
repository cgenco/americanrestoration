class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :services_menu, :affiliates_menu
  
  def services_menu
    @services ||= Servic.select('name')
  end
  
  def affiliates_menu
    @affiliate_categories ||= Affiliate.select('category').map(&:category).uniq
  end
end
