# # make RVM work
# $:.unshift(File.expand_path('./lib', ENV['rvm_path'])) # Add RVM's lib directory to the load path.
# require "rvm/capistrano"                  # Load RVM's capistrano plugin.
# set :rvm_ruby_string, 'ree@rails3'        # Or whatever env you want it to run in.
# set :rvm_type, :user  # Copy the exact line. I really mean :user here
# set :rvm_bin_path, "/usr/local/rvm/bin/"



# THIS SHOULD WORK
set :default_environment, {
  'PATH' => "/usr/local/rvm/gems/ruby-1.9.2-p0/bin:/usr/local/rvm/gems/ruby-1.9.2-p0@global/bin:/usr/local/rvm/rubies/ruby-1.9.2-p0/bin:/usr/local/rvm/bin:$PATH",
  'RUBY_VERSION' => 'ruby-1.9.2-p0',
  'GEM_HOME'     => '/usr/local/rvm/gems/ruby-1.9.2-p0',
  'GEM_PATH'     => '/usr/local/rvm/gems/ruby-1.9.2-p0:/usr/local/rvm/gems/ruby-1.9.2-p0@global',
  'BUNDLE_PATH'  => '/usr/local/rvm/gems/ruby-1.9.2-p0/'  # If you are using bundler.
}
namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end
after "deploy", "rvm:trust_rvmrc"


set :application, "AmericanRestoration"
set :repository,  "git@bitbucket.org:cgenco/americanrestoration.git"
set :scm, :git
set :scm_username, "cgenco"

# server "smuclasses.com", :app, :web, :db, :primary => true
server "americanrestorationinc.com", :app, :web, :db, :primary => true
set :user, "p4r81523"
set :deploy_to, "/home/p4r81523/rails"
set :use_sudo, false

thin_path = "/usr/local/rvm/gems/ruby-1.9.2-p0/bin/thin"
namespace :deploy do
  desc "Start the Thin processes"
  task :start do
    run "cd #{deploy_to}/current && bundle exec thin start -C thin/production_config.yml"
  end
  desc "Stop the Thin processes"
  task :stop do
    run "cd #{deploy_to}/current && bundle exec thin stop -C thin/production_config.yml"
  end
  desc "Restart the Thin processes"
  task :restart do
    run "cd #{deploy_to}/current && bundle exec thin restart -C thin/production_config.yml"
    # stop
    # start
  end
end

# sqlite3
set(:shared_database_path) {"#{shared_path}/databases"}
namespace :sqlite3 do
  desc "Generate a database configuration file"
  task :build_configuration, :roles => :db do
    db_options = {
      "adapter"  => "sqlite3",
      "database" => "#{shared_database_path}/production.sqlite3",
      "pool" => 5,
      "timeout" => 5000
    }
    config_options = {"production" => db_options}.to_yaml
    put config_options, "#{shared_database_path}/sqlite_config.yml"
  end
 
  desc "Links the configuration file"
  task :link_configuration_file, :roles => :db do
    run "ln -nsf #{shared_database_path}/sqlite_config.yml #{current_path}/config/database.yml"    
  end
 
  desc "Make a shared database folder"
  task :make_shared_folder, :roles => :db do
    run "mkdir -p #{shared_database_path}"
  end
end
after "deploy:setup", "sqlite3:make_shared_folder"
after "deploy:setup", "sqlite3:build_configuration"
after "deploy:update", "sqlite3:link_configuration_file"

# bundler
namespace :bundler do
  bundle_install_path = "vendor/bundle"
  bundle_path = "/usr/local/rvm/gems/ruby-1.9.2-p0/bin/bundle"
  # bundle --path vendor/bundle
  
  task :create_symlink, :roles => :app do
    shared_dir = File.join(shared_path, 'bundle')
    release_dir = File.join(release_path, bundle_install_path)
    run("mkdir -p #{shared_dir} && ln -s #{shared_dir} #{release_dir}")
  end
  
  task :install, :roles => :app do
    # --deployment ? http://gembundler.com/rationale.html
    
    # check if the Gemfile or Gemfile.lock changed
    unless capture("cat '#{previous_release}/Gemfile'") == capture("cat '#{release_path}/Gemfile'")
      puts "Gemfile was updated. Running bundle install."
      run "cd #{release_path} && #{bundle_path} :x#{bundle_install_path}"
    else
      puts "Gemfile was not updated. Skipping bundle install"
      # ignore errors
      run "rm #{release_path}/Gemfile.lock"
      run "cp #{previous_release}/Gemfile.lock #{release_path}/Gemfile.lock 2>/dev/null; echo lol"
    end

    on_rollback do
      if previous_release
        run "cd #{previous_release} && #{bundle_path} install --path #{bundle_install_path}"
      else
        logger.important "no previous release to rollback to, rollback of bundler:install skipped"
      end
    end
  end
  
  task :bundle_new_release, :roles => :db do
    bundler.create_symlink
    bundler.install
  end
end

after "deploy:rollback:revision", "bundler:install"
after "deploy:update_code", "bundler:bundle_new_release"