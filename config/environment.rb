# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
AmericanRestoration::Application.initialize!

# removes asset timestamp
ENV['RAILS_ASSET_ID'] = ''

# create static mirror with:
# wget -mk -nH http://localhost:3000