class RemovePhotoUrlFromTeamMember < ActiveRecord::Migration
  def self.up
    remove_column :team_members, :photo_url
  end

  def self.down
    add_column :team_members, :photo_url, :string
  end
end
