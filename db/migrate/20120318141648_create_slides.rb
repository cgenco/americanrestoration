class CreateSlides < ActiveRecord::Migration
  def self.up
    create_table :slides do |t|
      t.has_attached_file :image
      t.integer :servic_id
      t.timestamps
    end
  end

  def self.down
    drop_attached_file :slides, :image
    drop_table :slides
  end
end
