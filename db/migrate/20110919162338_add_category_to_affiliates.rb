class AddCategoryToAffiliates < ActiveRecord::Migration
  def self.up
    add_column :affiliates, :category, :string
  end

  def self.down
    remove_column :affiliates, :category
  end
end
