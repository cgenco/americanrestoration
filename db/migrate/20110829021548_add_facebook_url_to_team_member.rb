class AddFacebookUrlToTeamMember < ActiveRecord::Migration
  def self.up
    add_column :team_members, :facebook_url, :string
  end

  def self.down
    remove_column :team_members, :facebook_url
  end
end
