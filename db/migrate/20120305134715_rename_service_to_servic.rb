class RenameServiceToServic < ActiveRecord::Migration
  def self.up
    rename_table :services, :servics
  end

  def self.down
    rename_table :servics, :services
  end
end
