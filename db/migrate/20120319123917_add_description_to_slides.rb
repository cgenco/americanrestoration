class AddDescriptionToSlides < ActiveRecord::Migration
  def self.up
    add_column :slides, :description, :string
  end

  def self.down
    remove_column :slides, :description
  end
end
