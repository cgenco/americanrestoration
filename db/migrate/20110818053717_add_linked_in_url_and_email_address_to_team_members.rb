class AddLinkedInUrlAndEmailAddressToTeamMembers < ActiveRecord::Migration
  def self.up
    add_column :team_members, :linked_in_url, :string
    add_column :team_members, :email_address, :string
  end

  def self.down
    remove_column :team_members, :email_address
    remove_column :team_members, :linked_in_url
  end
end
