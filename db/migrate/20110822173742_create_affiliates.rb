class CreateAffiliates < ActiveRecord::Migration
  def self.up
    create_table :affiliates do |t|
      t.string :name
      t.string :url
      t.string :description
      t.string :logo
      t.string :screenshot

      t.timestamps
    end
  end

  def self.down
    drop_table :affiliates
  end
end
