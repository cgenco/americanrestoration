desc 'Prepare the static site in the ../static/ directory'
task :prepare  do
  Dir.mkdir('../static') unless File.exist?('../static')
  Dir.chdir('../static') do
    `wget -mk -nH -E http://localhost:3000`
  end
end

# wget -mk -nH -E http://www.americanrestorationinc.com.php5-14.dfw1-1.websitetestlink.com/