require 'time'

def screenshots
	screenshot_directory = Dir.new "/Users/cgenco/Dropbox/Public"
	screenshot_directory.entries.delete_if{|s| !s.index("Screen Shot 2012")}.sort_by{|filename|
    File.new(File.join screenshot_directory, filename).stat.ctime
  }.reverse.map{|filename|
    File.new(File.join screenshot_directory, filename)
  }
end

def rename_latest_screenshot name
  ar_screenshot_path = "/Users/cgenco/Dropbox/Projects/AmericanRestoration/american_restoration/public/images/affiliates/"
  File.rename screenshots.first, File.join(ar_screenshot_path, name)
end
