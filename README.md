# American Restoration Website

This website was created as a dynamic webapp in Rails to support a full Content Management System (CMS) in ActiveAdmin for all models and easy client-side editing, but can be compiled into a static site by running:

    bundle exec rake prepare

from the root directory. Note that the command `bundle install` will need to be run first, and all necessary dependencies (ruby, rails, etc.) will need to be properly configured.

Alternatively, the site may be edited in its static form. The statically compiled code may be found in the americanrestorationinc.com FTP folder `public_html`.

Please contact christian.genco@gmail.com with any questions regarding this code.# 